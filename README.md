# turbigen

**James Brind, Whittle Laboratory, University of Cambridge**

turbigen is a general turbomachinery design system. See the user manual at:

[User Manual](https://turbigen.org)
