import importlib.metadata

__version__ = importlib.metadata.version("turbigen")
__copyright__ = "2024 James Brind"
