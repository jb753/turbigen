"""Allow catching of clustering errors specifically."""


class ClusteringException(BaseException):
    pass
