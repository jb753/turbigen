class ConvergenceError(Exception):
    __module__ = Exception.__module__


class ConfigError(Exception):
    __module__ = Exception.__module__
