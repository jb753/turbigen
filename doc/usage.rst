.. _usage:

Command-line options
====================

.. argparse::
   :module: turbigen.main
   :func: _make_argparser
   :prog: turbigen
