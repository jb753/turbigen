:program:`turbigen` user manual
===============================

:program:`turbigen` is a general turbomachinery design system developed by `Dr. James Brind <https://jamesbrind.uk/>`_ of the `Whittle Laboratory <https://whittle.eng.cam.ac.uk/>`_, University of Cambridge.

This documentation contains
usage instructions, descriptions of the theory involved, and listings of configuration options.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   nomenclature
   install
   usage
   tutorial
   config
   fluid
   meanline
   mesh
   solver
   changelog
   references
   license

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
