Nomenclature
============

General conventions for variable names:

* Symbol or numeric subscripts are not separated by underscores: `Vx`, `Alpha1`.
* Accents and descriptive subscripts are separated by underscores: `Vt2_rel`, `mdot_stall`.
* Stagnation quantities are denoted by a subscript letter oh: `Po`, `ho_rel`.
* Depending on context, concatenation of symbols is either: a product, such as `rhoVx`; or a stack along the first array dimension, like `xrt`.

=========== ============================================ =======
 Symbol      Quantity                                     Units
=========== ============================================ =======
`a`          Acoustic Speed                               m/s
`cp`         Specific heat capacity at constant pressure  J/kg/K
`cv`         Specific heat capacity at constant volume    J/kg/K
`gamma`      Ratio of specific heats
`h`          Specific enthalpy                            J/kg
`mu`         Dynamic viscosity                            kg/m/s
`P`          Pressure                                     Pa
`Pr`         Prandtl number
`rgas`       Specific gas constant                        J/kg/K
`rho`        Mass density                                 kg/m^3
`s`          Specific entropy                             J/kg/K
`T`          Temperature                                  K
`u`          Specific internal energy                     J/kg
`x`          Axial coordinate                             m
`r`          Radial coordinate                            m
`t`          Tangential coordinate                        rad
`m`          Meridional coordinate                        m
`Omega`      Shaft angular velocity                       rad/s
`y`          Cartesian vertical coordinate                m
`z`          Cartesian horizontal coordinate              m
`spf`        Span fraction coordinate
`V`          Velocity                                     m/s
`mdot`       Mass flow rate                               kg/s
`Alpha`      Yaw angle                                    deg
`Beta`       Pitch angle                                  deg
`U`          Blade speed                                  m/s
`e`          Specific total energy                        J/kg
`I`          Rothalpy                                     J/kg
`Ma`         Mach number
`A`          Area                                         m^2
`zeta`       Surface distance coordinate                           m^2
=========== ============================================ =======
